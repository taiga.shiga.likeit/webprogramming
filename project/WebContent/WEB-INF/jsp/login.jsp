<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!--  EL式使用用 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="ja">
<meta charset="UTF-8">
<title>Login</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>

<body style="background-color: antiquewhite">
	<h1
		style="font-size: 55px; color: blueviolet; text-align: center; margin-top: 100px;">ログイン画面</h1>

	<div style="text-align: center; padding-top: 20px;">
		<img src="person.jpeg" style="border-radius: 50%;">
	</div>

	<!--       acitonのところはどこのサーブレットを使うのかなどを間違わないように記述 -->
	<form class="form-signin" action="LoginServlet" method="post">
		<h3 style="color: black; text-align: center; margin-top: 100px">ログインIDを入力ください</h3>
		<div style="margin-top: 20px;" class="row justify-content-center">
			<div class="col-4">
				<!-- 		login id -->
				<input type="text" name="loginId"
					style="width: 100%; border-radius: 20px;" placeholder="Your ID"
					class="form-control">
			</div>
		</div>

		<!--  もしログイン情報が間違ってたりデータがなかったら -->
		<c:if test="${errMsg != null}">
			<div role="alert" style= "color: red; font-size: 20px; padding-top: 20px; text-align: center">${errMsg}</div>
		</c:if>

		<h3 style="color: black; text-align: center; margin-top: 100px">パスワードを入力ください</h3>
		<div style="margin-top: 20px;" class="row justify-content-center">
			<div class="col-4">
				<!-- password -->
				<input type="text" name="password"
					style="width: 100%; border-radius: 20px;"
					placeholder="Your Password" class="form-control">
			</div>
		</div>
		<div style="margin-top: 50px; margin-bottom: 50px;"
			class="row justify-content-center">
			<div>
				<button type="submit" style="width: 200px; height: 50px"
					class="btn btn-primary">ログイン</button>
			</div>
		</div>
	</form>

</body>
</html>
