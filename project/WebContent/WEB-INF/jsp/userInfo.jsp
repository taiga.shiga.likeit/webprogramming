<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang = "ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザー情報詳細</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<body style="margin: 0;">

    <div style="width: 100%; height: 80px; background-color: antiquewhite; margin: 0;">
        <div style="padding-right: 50">
            <!-- ログアウト先はログイン画面-->
            <a href="LogoutServlet">
                <p style="text-align: right; float: right; padding-top: 20px; padding-right: 10px; font-size: 20px">
                    ログアウト</p>
            </a>
            <p style="margin: 0; padding-top: 15px; font-size: 25px; text-align: right; float: right; padding-right: 100px;">${userInfo.name}さん
            </p>
        </div>
    </div>

    <h1 style="text-align: center; font-size: 40px; padding-top: 30px;">ユーザー情報詳細</h1>
    <div style="padding-top:100px" class="row justify-content-center">
        <div class="col-4">
            <p style="text-align: center; font-size: 25px">ログインID</p>
        </div>
        <div class="col-4">
            <p style="text-align: center; font-size: 25px">${user.loginId}</p>
        </div>
    </div>


    <div style="padding-top:75px" class="row justify-content-center">
        <div class="col-4">
            <p style="text-align: center; font-size: 25px;">ユーザー名</p>
        </div>
        <div class="col-4">
            <p style="text-align: center; font-size: 25px;">${user.name}</p>
        </div>
    </div>

    <div style="padding-top:75px" class="row justify-content-center">
        <div class="col-4">
            <p style="text-align: center; font-size: 25px;">生年月日</p>
        </div>
        <div class="col-4">
            <p style="text-align: center; font-size: 25px;">
            ${user.birthDate}
            </p>
        </div>
    </div>

    <div style="padding-top:75px" class="row justify-content-center">
        <div class="col-4">
            <p style="text-align: center; font-size: 25px;">登録日時</p>
        </div>
        <div class="col-4">
            <p style="text-align: center; font-size: 25px">${user.createDate}</p>
        </div>
    </div>

    <div style="padding-top:75px" class="row justify-content-center">
        <div class="col-4">
            <p style="text-align: center; font-size: 25px;">更新日時</p>
        </div>
        <div class="col-4">
            <p style="text-align: center; font-size: 25px;">${user.updateDate}</p>
        </div>
    </div>

    <!--    ここに戻る先のurl-->
    <a href="UserListServlet">
        <p style="text-align: left; padding-left: 50px; padding-top: 100px; font-size: 30px;">戻る</p>
    </a>

</body></html>

