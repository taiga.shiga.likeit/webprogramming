<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<title>ユーザー覧</title>
</head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">

<body style="margin: 0;">

	<div
		style="width: 100%; height: 80px; background-color: antiquewhite; margin: 0;">
		<div style="padding-right: 50">
			<!-- ログアウト先はログイン画面-->
			<a href="LogoutServlet">
				<p
					style="text-align: right; float: right; padding-top: 20px; padding-right: 10px; font-size: 20px">
					ログアウト</p>
			</a>
			<!-- ここでのuserInfoはセッションのため使うことができる -->
			<p
				style="margin: 0; padding-top: 15px; font-size: 25px; text-align: right; float: right; padding-right: 100px;">${userInfo.name}さん</p>
		</div>
	</div>

	<a href="UserCreateServlet">
		<p
			style="text-align: right; float: right; padding-top: 20px; padding-right: 100px; font-size: 20px;">
			新規登録</p>
	</a>

	<h1 style="text-align: center; font-size: 40px; padding-top: 50px;">ユーザー覧</h1>

	<form action="UserListServlet" method="post">
	<div style="padding-top: 50px" class="row justify-content-center">
		<div class="col-4">
			<p style="text-align: center; font-size: 25px">ログインID</p>
		</div>
		<div class="col-4">
			<input type="text" style="width: 75%; height: 40px; font-size: 20px;"
				placeholder="loginID" class="form-control" name="loginId">
		</div>
	</div>

	<div style="padding-top: 75px" class="row justify-content-center">
		<div class="col-4">
			<p style="text-align: center; font-size: 25px;">ユーザー名</p>
		</div>
		<div class="col-4">
			<input type="text" style="width: 75%; height: 40px; font-size: 20px;"
				placeholder="User name" class="form-control" name="userName">
		</div>
	</div>

	<div style="padding-top: 75px" class="row justify-content-center">
		<div class="col-2">
			<p style="text-align: center; font-size: 25px;">生年月日</p>
		</div>
		<div class="col-2">
			<input type="date"
				style="width: 100%; height: 40px; font-size: 15px;"
				placeholder="Your Birthday" class="form-control" name="startBd">
		</div>
		<p Style="padding-right: 20px; padding-left: 20px">〜</p>
		<div class="col-2">
			<input type="date"
				style="width: 100%; height: 40px; font-size: 15px;"
				placeholder="Your Birthday" class="form-control" name="finishBd">
		</div>
	</div>

	<div style="margin-top: 50px; text-align: right; padding-right: 100px;">
		<div>
			<button value="検索" type="submit"
				style="width: 200px; height: 50px; background-color: darkseagreen; font-size: 20px;"
				class="btn btn-primary">検索</button>
		</div>
	</div>
</form>

	<p
		style="text-align: center; width: 100%; height: 2px; background-color: black; margin-top: 30px"></p>

	<table border="1" align="center" style="margin-bottom: 50px">
		<tr>
			<th style="width: 200px">ログインID</th>
			<th style="width: 200px">ユーザ名</th>
			<th style="width: 200px">生年月日</th>
			<th style="width: 350px"></th>
		</tr>

		<!-- 		拡張for文 -->
		<c:forEach var="user" items="${userList }">
			<tr>
				<c:if test="${userInfo.name == '管理者'}" var="flg" />
				<c:if test="${flg}">
					<td>${user.loginId }</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td><a href="UserDetailServlet?id=${user.id}"><input
							type="button" value="詳細" class="btn btn-primary"
							style="background-color: blue; width: 100px; margin-right: 20px; margin-bottom: 10px; margin-top: 5px;"></a>
						<a href="UserUpdateServlet?id=${user.id}"><input type="button"
							value="更新" class="btn btn-primary"
							style="background-color: mediumpurple; width: 100px; margin-right: 20px; margin-bottom: 10px; margin-top: 5px;"></a>

						<a href="UserDeleteServlet?id=${user.id}"><input type="button"
							value="削除" class="btn btn-primary"
							style="background-color: red; width: 100px; margin-bottom: 10px; margin-top: 5px"></a>
					</td>
				</c:if>

				<c:if test="${!flg}">
					<td>${user.loginId }</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<c:if test="${user.name == userInfo.name}" var="youIs" />
					<c:if test="${youIs}">
						<td><a href="UserDetailServlet?id=${user.id}"><input
								type="button" value="詳細" class="btn btn-primary"
								style="background-color: blue; width: 100px; margin-right: 20px; margin-bottom: 10px; margin-top: 5px;"></a>
							<a href="UserUpdateServlet?id=${user.id}"><input
								type="button" value="更新" class="btn btn-primary"
								style="background-color: mediumpurple; width: 100px; margin-right: 20px; margin-bottom: 10px; margin-top: 5px;"></a>
							<a href="UserDeleteServlet?id=${user.id}"><input
								type="button" value="削除" class="btn btn-primary"
								style="background-color: red; width: 100px; margin-bottom: 10px; margin-top: 5px"></a>
						</td>
					</c:if>

					<c:if test="${!youIs}">
						<td><a href="UserDetailServlet?id=${user.id}"><input
								type="button" value="詳細" class="btn btn-primary"
								style="background-color: blue; width: 100px; margin-right: 20px; margin-bottom: 10px; margin-top: 5px;"></a>
					</c:if>
				</c:if>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
