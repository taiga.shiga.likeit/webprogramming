<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザー情報アップデート</title>
</head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">

<body style="margin: 0;">

	<div
		style="width: 100%; height: 80px; background-color: antiquewhite; margin: 0;">
		<div style="padding-right: 50">
			<!-- ログアウト先はログイン画面-->
			<a href="LogoutServlet">
				<p
					style="text-align: right; float: right; padding-top: 20px; padding-right: 10px; font-size: 20px">
					ログアウト</p>
			</a>
			<p
				style="margin: 0; padding-top: 15px; font-size: 25px; text-align: right; float: right; padding-right: 100px;">${userInfo.name}さん</p>
		</div>
	</div>

	<h1 style="text-align: center; font-size: 40px; padding-top: 30px;">ユーザー情報をアップデート</h1>

	<form action="UserUpdateServlet" method="post">

		<div style="padding-top: 100px" class="row justify-content-center">
			<div class="col-4">
				<p style="text-align: center; font-size: 25px">ログインID</p>
			</div>
			<div class="col-4">
				<input type="text" style="width: 75%; height: 40px; font-size: 30px"
					class="form-control" name="loginId" value="${user.loginId}"
					readonly>
			</div>
		</div>

		<div style="padding-top: 75px" class="row justify-content-center">
			<div class="col-4">
				<p style="text-align: center; font-size: 25px;">パスワード</p>
			</div>
			<div class="col-4">
				<input type="text"
					style="width: 75%; height: 40px; font-size: 20px;"
					placeholder="Password" class="form-control" name="password">
			</div>
		</div>

		<div style="padding-top: 75px" class="row justify-content-center">
			<div class="col-4">
				<p style="text-align: center; font-size: 25px;">パスワード確認</p>
			</div>
			<div class="col-4">
				<input type="text"
					style="width: 75%; height: 40px; font-size: 20px;"
					placeholder="Password" class="form-control" name="checkPassword">
			</div>
		</div>

		<div style="padding-top: 75px" class="row justify-content-center">
			<div class="col-4">
				<p style="text-align: center; font-size: 25px;">ユーザー名</p>
			</div>
			<div class="col-4">
				<input type="text"
					style="width: 75%; height: 40px; font-size: 20px;"
					value="${user.name}" placeholder="Name" class="form-control"
					name="userName">
			</div>
		</div>

		<div style="padding-top: 75px" class="row justify-content-center">
			<div class="col-4">
				<p style="text-align: center; font-size: 25px;">生年月日</p>
			</div>
			<div class="col-4">
				<input type="date"
					style="width: 75%; height: 40px; font-size: 20px;"
					value="${user.birthDate}" class="form-control" name="birthDate">
			</div>
		</div>


		<c:if test="${errMsg1 != null}">
			<div role="alert">
				<p
					style="color: red; font-size: 20px; padding-top: 20px; text-align: center">${errMsg1}</p>
			</div>
		</c:if>

		<c:if test="${errMsg2 != null}">
			<div role="alert">
				<p
					style="color: red; font-size: 20px; padding-top: 20px; text-align: center">${errMsg2}</p>
			</div>
		</c:if>
	<c:if test="${errMsg3 != null}">
			<div role="alert">
				<p
					style="color: red; font-size: 20px; padding-top: 20px; text-align: center">${errMsg3}</p>
			</div>
		</c:if>
		<div style="margin-top: 50px" class="row justify-content-center">
			<div>
				<button type="submit"
					style="width: 200px; height: 50px; background-color: darkseagreen"
					class="btn btn-primary">更新</button>
			</div>
		</div>
	</form>


	<!--    ここに戻る先のurl-->
	<a href="UserListServlet">
		<p
			style="text-align: left; padding-left: 50px; padding-top: 100px; font-size: 30px;">戻る</p>
	</a>

</body>
</html>

