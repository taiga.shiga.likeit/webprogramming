<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>削除画面</title>
</head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">

<body style="margin: 0;">

	<div
		style="width: 100%; height: 80px; background-color: antiquewhite; margin: 0;">
		<div style="padding-right: 50">
			<!-- ログアウト先はログイン画面-->
			<a href="LoginServlet">
				<p
					style="text-align: right; float: right; padding-top: 20px; padding-right: 10px; font-size: 20px">ログアウト</p>
			</a>
			<p
				style="margin: 0; padding-top: 15px; font-size: 25px; text-align: right; float: right; padding-right: 100px;">${userInfo.name}さん
			</p>
		</div>
	</div>

<form action="UserDeleteServlet" method="post">
	<h1
		style="text-align: center; padding-top: 50px; color: black; font-size: 50px">ユーザー情報削除確認</h1>
	<div style="padding-top: 100px; text-align: center">
		<p style="font-size: 40px">ログインID:${user.loginId}</p>
		<p style="font-size: 40px">本当に消してよろしいですか？</p>
	</div>

	<div style="padding-top: 75px" class="row justify-content-center">

		<div class="col-4">
			<a href="UserListServlet"><button
					style="width: 100%; height: 50px" type="button"
					class="btn btn-primary">キャンセル</button></a>
		</div>
			<div class="col-4">
				<button style="width: 100%; height: 50px;" type="submit"
					class="btn btn-primary" value="${user.loginId}" name="loginId">OK</button>
			</div>
	</div>
</form>
</body>
</html>

