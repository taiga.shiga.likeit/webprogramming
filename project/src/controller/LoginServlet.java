package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}
	// login画面へのパス
	String LOGINPATH= "/WEB-INF/jsp/login.jsp";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 *
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる

		// 遷移準備
		RequestDispatcher dispatcher = request.getRequestDispatcher(LOGINPATH);
		// ログイン画面に forward
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		// 確認用
		System.out.println("loginid is " + loginId);
		System.out.println("password is" + password);

		//daoの呼び出し
		UserDao userDao = new UserDao();
		// リクエストパラメーターのを引数に渡す
		// ログインした人の情報を確認するため
		User user = userDao.findByLoginInfo(loginId, password);

		// テーブルに該当のデータがない場合
		if (user == null) {
			// リクエストスコープにエラーをだす
			request.setAttribute("errMsg", "ログインに失敗しました。もう一度お願いします");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(LOGINPATH);
			dispatcher.forward(request, response);
			// 処理を返す
			return;
		}
		HttpSession session = request.getSession();
		// セッションにuserdataを入れる
		session.setAttribute("userInfo", user);
		// ユーザー欄のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");
	}
}
