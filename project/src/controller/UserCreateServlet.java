package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	String createPath = "/WEB-INF/jsp/userResister.jsp";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 新規登録画面に移動
		RequestDispatcher dispatcher = request.getRequestDispatcher(createPath);
		// forward
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String userName = request.getParameter("userName");
	    String yourbirthday = request.getParameter("birthday");
	    String confromPassword = request.getParameter("checkPassword");

	   if (loginId.equals("") || password.equals("") || userName.equals("") || yourbirthday.equals("")) {
		   request.setAttribute("errMsg", "入力されていない項目があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userResister.jsp");
			dispatcher.forward(request, response);
			return;

	   }else if (!password.equals(confromPassword)) {
		   request.setAttribute("errMsg2", "確認用パスワードが一致しません");
	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userResister.jsp");
			dispatcher.forward(request, response);
		    return;
	   }


	// string型になっているのでSQLのdate型にキャストする
	    Date birthday = Date.valueOf(yourbirthday);
	   // userdao呼ぶ
	   UserDao userDao = new UserDao();

	   String checkId = userDao.idExist(loginId);

	   System.out.println(checkId);

	   if(checkId != null){
		   request.setAttribute("errMsg3", "idが重複してます");
	       RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userResister.jsp");
		   dispatcher.forward(request, response);
		   return;
	   }

	   // userdaoのcreateAccount()を呼ぶ
	   int userCreate = userDao.createAccount(loginId, password, userName, birthday);

	   System.out.println(userCreate);

		response.sendRedirect("UserListServlet");

	}

}
