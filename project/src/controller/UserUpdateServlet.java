package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	String UPDATE = "/WEB-INF/jsp/userUpdate.jsp";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		int uid = Integer.parseInt(id);

		UserDao userDao = new UserDao();
		User user = userDao.userInfo(uid);
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher(UPDATE);
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginid = request.getParameter("loginId");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String userName = request.getParameter("userName");
		String yourBirthday = request.getParameter("birthDate");

		if (userName.equals("")&&yourBirthday.isEmpty()){
			User user = new User(loginid, userName);
			//内容をセット
			request.setAttribute("errMsg1", "入力されていない項目があります");
			request.setAttribute("user", user);
			// jspへ遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher(UPDATE);
			dispatcher.forward(request, response);
			// 処理通ってる確認用
			System.out.println("入力されなかった");
			return;
		}else if (yourBirthday.isEmpty() || userName.equals("")) {
			User user = new User(loginid, userName);
			request.setAttribute("errMsg2", "入力されていない項目があります");
			request.setAttribute("user", user);
			// jspへ遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher(UPDATE);
			dispatcher.forward(request, response);
			// 処理通ってる確認用
			System.out.println("誕生日がない");
			return;
		}else if (!password.equals(checkPassword)) {
			Date birthday = Date.valueOf(yourBirthday);
			User user  = new User(loginid, userName, birthday);
			request.setAttribute("errMsg3", "確認用パスワードと合致しません");
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher(UPDATE);
			dispatcher.forward(request, response);
			// 処理通ってるか確認用
			System.out.println("パスワード違う");
			return;
		}

		// string型になっているのでSQLのdate型にキャストする
	   Date birthday = Date.valueOf(yourBirthday);
	   // userdao呼ぶ
	   UserDao userDao = new UserDao();
	   userDao.update(userName, password, birthday, loginid);
	  response.sendRedirect("UserListServlet");

	}

}
