package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			String yourPassword = pwEncryption(password);

			pStmt.setString(1, loginId);
			pStmt.setString(2, yourPassword);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id NOT IN (1)";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate,
						updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public List<User> findAll(String loginUserId,String userName,String startDate, String endDate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id NOT IN (1)";

			if (!loginUserId.equals("")) {
				// loginid を完全一致
				sql += " AND login_id = '" + loginUserId + "'";
			}
			if (!userName.equals("")) {
				// 名前を部分一致
				sql += "AND name like '%" + userName + "%'";
			}
			if (!startDate.equals("")) {
				// 誕生日期間の指定
			   sql += " AND birth_date >= '" + startDate + "'";
			}
			if (!endDate.equals("")) {
				// 誕生日の期間の指定
				sql += "AND birth_date <= '" + endDate + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate,
						updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public int createAccount(String loginId, String password, String userName, Date birthday) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) VALUES (?,?,?,?,now(),now());";

			// insertを実行
			PreparedStatement stmt = con.prepareStatement(sql);

			String yourPassword = pwEncryption(password);
			stmt.setString(1, loginId);
			stmt.setString(2, userName);
			stmt.setDate(3, birthday);
			stmt.setString(4, yourPassword);
			// 戻りがint型 sqlの実行 登録されたレコード数が帰ってくる
			int result = stmt.executeUpdate();
			// resultが1なら登録が成功したということ
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String idExist(String loginId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ?";
			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();
			//
			if (!rs.next()) {
				return null;
			}
			return "idが存在しました";

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	// userの詳細
	public User userInfo(int id) {
		Connection con = null;
		User user = null;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id =" + id;

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int uid = id;
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				user = new User(uid, loginId, name, birthDate, password, createDate, updateDate);
			}

		} catch (Exception e) {
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return user;
	}

	public void update(String name, String password, Date birthday, String loginId) {
		Connection con = null;
		int result = 0;
		try {
			con = DBManager.getConnection();
			if (password.isEmpty()) {
				String sql = "UPDATE user SET name = ? , birth_date = ?, update_date = now() where login_id = ?";
				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = con.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setDate(2, birthday);
				pStmt.setString(3, loginId);
				// update
				pStmt.executeUpdate();
			} else {
				String sql = "UPDATE user SET name = ? , password = ?, birth_date = ?, update_date = now() where login_id = ?";
				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = con.prepareStatement(sql);
				String yourPassword = pwEncryption(password);
				pStmt.setString(1, name);
				pStmt.setString(2, yourPassword);
				pStmt.setDate(3, birthday);
				pStmt.setString(4, loginId);
				pStmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void userDataDelete(String loginId) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String sql = "DELETE FROM user WHERE login_id = ?";
			// deleteを実行
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			//実行
			pStmt.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	 }

	public String pwEncryption(String password) throws NoSuchAlgorithmException {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result ;
	}

}
